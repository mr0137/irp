﻿using System;
using System.Runtime.InteropServices;

namespace PIR1
{
    public static class Imports
    {
        [DllImport("TreeNode.dll")]
        public static extern IntPtr CreateTreeManager();

        [DllImport("TreeNode.dll")]
        public static extern bool DeleteTreeManager(IntPtr treeManager);

        [DllImport("TreeNode.dll")]
        public static extern IntPtr CreateRootNode(IntPtr treeManager);

        [DllImport("TreeNode.dll")]
        public static extern bool DeleteRootNode(IntPtr treeManager);

        [DllImport("TreeNode.dll")]
        public static extern IntPtr GetRootNode(IntPtr treeManager);

        [DllImport("TreeNode.dll")]
        public static extern int GetChildrensCount(IntPtr treeNode);

        [DllImport("TreeNode.dll")]
        public static extern bool DeleteTreeNode(IntPtr treeNode);

        [DllImport("TreeNode.dll")]
        public static extern IntPtr AddChild(IntPtr treeManager, IntPtr treeNode, string name);

        [DllImport("TreeNode.dll")]
        public static extern IntPtr GetName(IntPtr treeNode);

        [DllImport("TreeNode.dll")]
        public static extern int GetDepth(IntPtr treeNode);
    }
}
