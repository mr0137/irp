﻿using System;

namespace PIR1
{
    class Program
    {
        static void Main(string[] args)
        {
            var manager = new TreeManager();
            manager.CreateDefaultTree();

            Console.CursorVisible = false;
            bool running = true;
            int current_pos = 0;

            void incPos()
            {
                if (manager.GetChildrensCount() + 1 > current_pos + 1)
                {
                    current_pos++;
                }
            }

            void decPos()
            {
                if (current_pos - 1 >= 0)
                {
                    current_pos--;
                }
            }

            void addNode()
            {
                displayEnterName();
            }

            void removeNode()
            {
                manager.DeleteTreeNode(current_pos);
                decPos();
            }

            void displayTree()
            {
                Console.Clear();
                Console.WriteLine("Controls:");
                Console.WriteLine("Arrow UP:\t move current pos up");
                Console.WriteLine("Arrow DOWN:\t move current pos down");
                Console.WriteLine("Key +:\t\t add child to node");
                Console.WriteLine("Key -:\t\t remove node");
                Console.WriteLine("Escape:\t\t exit\n");
                var childList = manager.GetChildrensList();
                int pos = 0;
                for (int i = 0; i < childList.Length; i++)
                {
                    if (childList[i] == '\n')
                    {
                        if (pos++ >= current_pos)
                        {
                            childList = childList.Insert(i-1, "  <--");
                            break;
                        }
                    }
                }
                Console.Write(childList);
            }

            void displayEnterName()
            {
                Console.Clear();
                Console.Write("Enter name:");
                var name = Console.ReadLine();
                if (name == "Root")
                {
                    Console.WriteLine("You can not use name Root!");
                    Console.WriteLine("Press any button to try again.");
                    Console.ReadKey();
                    displayEnterName();
                    return;
                }
                manager.AddChild(current_pos, name);
            }

            while (running)
            {
                displayTree();
                ConsoleKeyInfo _Key = Console.ReadKey();
                switch (_Key.Key)
                {
                    case ConsoleKey.UpArrow:
                        decPos();
                        break;
                    case ConsoleKey.DownArrow:
                        incPos();
                        break;
                    case ConsoleKey.OemPlus:
                        addNode();
                        break;
                    case ConsoleKey.OemMinus:
                        removeNode();
                        break;
                    case ConsoleKey.Escape:
                        running = false;
                        break;
                    default:
                        break;
                }
            }

            manager.Dispose();
        }
    }
}
