using Microsoft.VisualStudio.TestTools.UnitTesting;
using PIR1;

using System;

namespace TESTS
{
    [TestClass]
    public class TreeManagerTest
    {
        [AssemblyInitialize()]
        public static void AssemblyInit(TestContext context)
        {
            System.Environment.CurrentDirectory += "\\..\\";
        }

        [TestMethod]
        public void CreateTreeTest1()
        {
            TreeManager manager = new TreeManager();

            manager.CreateDefaultTree();
            manager.AddChild(0, "Test");

            Assert.AreEqual(manager.GetName(manager.GetChildrensCount()), "Test", "String are not equal");
        }

        [TestMethod]
        public void CreateTreeTest2()
        {
            TreeManager manager = new TreeManager();

            manager.CreateDefaultTree();
            Assert.AreEqual(manager.GetName(1), "Node1", "String are not equal");
        }
    }
}
