#pragma once

#include "lib_global.h"
#include <vector>

//avoid error: "std::vector needs to have dll-interface to be used by clients of class"
#pragma warning(disable: 4251 6295)

class TreeManager;

class LIBMANAGER_DECL TreeNode
{
    friend class TreeManager;
public:
    TreeNode(const char* name, int uid, TreeNode* parent = nullptr);
    ~TreeNode();

    TreeNode* addChildren(const char* name, int uid);
     
    const char* getName();
    int getDepth() const;
    int getUID() const;
    int getChildrensCount() const;

private:
    bool removeChildrenFromVector(TreeNode* node);

private:
    int m_depth = 0;
    int m_uid = 0;
    TreeNode* m_parent = nullptr;
    std::string m_name;
    std::vector<TreeNode*> m_childrens;
};

#pragma region wrapper
EXP int GetChildrensCount(TreeNode* node);
EXP const char* GetName(TreeNode* node);
EXP int GetDepth(TreeNode* node);
EXP bool DeleteTreeNode(TreeNode* node);
#pragma endregion wrapper

