#include "pch.h"
#include "TreeManager.h"
#include "TreeNode.h"

TreeNode* TreeManager::createRootNode()
{
    if (m_root == nullptr)
    {
        m_uid = 1;
        m_root = new TreeNode("Root", 0);
    }

    return m_root;
}

TreeManager::~TreeManager()
{
    if (m_root != nullptr)
    {
        delete m_root;
        m_root = nullptr;
    }
}

bool TreeManager::deleteRootNode()
{
    if (m_root != nullptr)
    {
        delete m_root;
        m_root = nullptr;
        return true;
    }
    return false;
}

bool TreeManager::deleteTreeNode(TreeNode* node)
{
    if (node == nullptr)
    {
        return false;
    }

    if (contains(node, m_root))
    {
        delete node;
        node = nullptr;
        return true;
    }
    return false;
}

TreeNode* TreeManager::addChild(TreeNode* node, const char* name)
{
    return node->addChildren(name, m_uid++);
}

bool TreeManager::contains(TreeNode* node, TreeNode* root)
{
    if (node == root)
    {
        return true;
    }

    for (auto* n : root->m_childrens)
    {
        if (contains(node, n))
        {
            return true;
        }
    }
    return false;
}

TreeNode* TreeManager::getRootNode()
{
    return m_root;
}

TreeManager* CreateTreeManager()
{
    return new TreeManager();
}

void DeleteTreeManager(TreeManager* pObject)
{
    if (pObject != nullptr)
    {
        delete pObject;
        pObject = nullptr;
    }
}

TreeNode* CreateRootNode(TreeManager* pObject)
{
    if (pObject != nullptr)
    {
        return pObject->createRootNode();
    }
    return nullptr;
}

bool DeleteRootNode(TreeManager* pObject)
{
    if (pObject != nullptr)
    {
        return pObject->deleteRootNode();
    }
    return false;
}

TreeNode* GetRootNode(TreeManager* pObject)
{
    if (pObject != nullptr) 
    {
        return pObject->getRootNode();
    }
    return nullptr;
}

TreeNode* AddChild(TreeManager* pObject, TreeNode* node, const char* name)
{
    if (pObject != nullptr && node != nullptr)
    {
       return pObject->addChild(node, name);
    }
    return nullptr;
}

