#include "pch.h"
#include "TreeNode.h"

TreeNode::TreeNode(const char* name, int uid, TreeNode* parent)
    : m_name(name),
    m_uid(uid)
{
    if (parent == nullptr)
    {
        m_depth = 0;
    }
    else
    {
        m_parent = parent;
        m_depth = parent->getDepth() + 1;
    }
}

TreeNode::~TreeNode()
{
    if (m_parent != nullptr)
    {   
        m_parent->removeChildrenFromVector(this);
    }

    for (int i = static_cast<int>(m_childrens.size()) - 1; i >= 0; i--)
    {
        delete m_childrens[i];
    }
    m_childrens.clear();
}

TreeNode* TreeNode::addChildren(const char* name, int uid)
{
    auto treeNode = new TreeNode(name, uid, this);
    m_childrens.push_back(treeNode);
    return treeNode;
}

const char* TreeNode::getName()
{
    return m_name.c_str();
}

int TreeNode::getDepth() const
{
    return m_depth;
}

int TreeNode::getUID() const
{
    return m_uid;
}

int TreeNode::getChildrensCount() const
{
    int childrensCount = 0;

    for (const auto* child : m_childrens)
    {
        childrensCount += child->getChildrensCount();
    }
    return childrensCount + static_cast<int>(m_childrens.size());
}

bool TreeNode::removeChildrenFromVector(TreeNode* node)
{
    for (int i = static_cast<int>(m_childrens.size()) - 1; i >= 0; i--)
    {
        if (node == m_childrens[i])
        {
            m_childrens.erase(m_childrens.begin() + i);
            return true;
        }
    }
    return false;
}

int GetChildrensCount(TreeNode* node)
{
    if (node != nullptr)
    {
        return node->getChildrensCount();
    }
    return -1;
}

bool DeleteTreeNode(TreeNode* node)
{
    if (node != nullptr)
    {
        delete node;
        node = nullptr;
        return true;
    }
    return false;
}

const char* GetName(TreeNode* node)
{
    if (node != nullptr)
    {
        return node->getName();
    }
    return nullptr;
}

int GetDepth(TreeNode* node)
{
    if (node != nullptr)
    {
        return node->getDepth();
    }
    return -1;
}
