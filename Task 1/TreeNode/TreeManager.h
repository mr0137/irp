#pragma once

#include "lib_global.h"

#pragma warning(disable: 4251)

class TreeNode;

class LIBMANAGER_DECL TreeManager
{
public:
    TreeManager() = default;
    ~TreeManager();

    TreeNode* createRootNode();
    TreeNode* getRootNode();
    bool deleteRootNode();

    bool deleteTreeNode(TreeNode* node);
    TreeNode* addChild(TreeNode* node, const char* name);
    bool contains(TreeNode* node, TreeNode* root);

private:
    TreeNode* m_root = nullptr;
    int m_uid = 0;
};

#pragma region wrapper
EXP TreeManager* CreateTreeManager();
EXP void DeleteTreeManager(TreeManager* pObject);

EXP TreeNode* CreateRootNode(TreeManager* pObject);
EXP bool DeleteRootNode(TreeManager* pObject);
EXP TreeNode* GetRootNode(TreeManager* pObject);

EXP TreeNode* AddChild(TreeManager* pObject, TreeNode* node, const char* name);
#pragma endregion wrapper