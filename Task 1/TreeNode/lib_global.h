#pragma once

#include <windows.h>

#ifdef TREENODE_EXPORTS
#define LIBMANAGER_DECL __declspec(dllexport)
#else
#define LIBMANAGER_DECL __declspec(dllimport)
#endif

#define EXP extern "C" LIBMANAGER_DECL
