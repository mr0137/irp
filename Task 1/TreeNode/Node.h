#pragma once

#include "lib_global.h"
#include <vector>

#pragma warning(disable: 4251 6295)

class LIBMANAGER_DECL Node
{
public:
    Node(const char* name, Node *parent = nullptr); 
    ~Node();

    void init(std::vector<std::string> &cfg, int depth = 1);

    const char* getName();
    int childrenCount();

    Node* getChildren(int pos);
    Node* addChildren();
    //depth-first traversal searching entry point
    Node* find(int pos);

    void removeChildren(int pos);
    void removeChildren(Node* node);
    void removeAll();

    void deleteItself();
    //returning list of all nodes
    void getChildrens(std::string& list);

    const char* saveChildList(const std::string& data);
    void releaseChildList();

private:
    Node* m_parent = nullptr;
    int m_depth = -1;
    char* m_childList;
    std::string m_name;
    std::vector<Node*> m_childrens;
    //depth-first traversal searching
    Node* findRec(int &pos);
};

#pragma region wrapper

EXP const char* GetChildrensList(Node* node);
EXP void ReleaseChildrenList(Node* node);
EXP Node* GetChildren(Node* node, int pos);
EXP int GetChildrensCount(Node* node);
EXP void RemoveNode(Node* node);
EXP void AddChild(Node* node);
EXP const char* GetName(Node* node);

#pragma endregion wrapper