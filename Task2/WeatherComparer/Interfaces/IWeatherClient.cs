﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace WeatherComparer 
{
    public abstract class IWeatherClient
    {
        public abstract Task<PointF> GetCityInfoAsync(string country);
        public abstract Task<List<Result>> GetWeatherInfoAsync(string city, int days);
        public abstract Result ParseJson(string jsonResponse);
    }
}
