﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherComparer
{
    public abstract class IWeatherManager
    {
        public abstract WeatherComparisonResults GetResults(string city1, string city2, int days);
    }
}
