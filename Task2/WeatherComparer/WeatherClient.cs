﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace WeatherComparer
{
    public class WeatherClient : IWeatherClient
    {
        private const string m_openWeatherApiKey = "ed3fb09bdeff6c637b165ec16bd56f00";
        private const string m_positionApiKey = "d6c8319849a1c7fd47ba93e4b616f73b";
        private readonly long m_dayUnixTime = 86400;
        private HttpClient m_httpClient;
        private long getCurrentTime() => (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
        public static DateTime getDateTimeFromStamp(long unixTimeStamp)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dateTime;
        }
        private Uri generateOpenWeatherRequestUrl(PointF coords, long timeStamp) => new Uri($"https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=" + coords.X + "&lon=" + coords.Y + "&dt=" + timeStamp + "&units=metric&appid=" + m_openWeatherApiKey);
        private Uri generatePositionRequestUrl(string address) => new Uri($"http://api.positionstack.com/v1/forward?access_key={m_positionApiKey}&query={address}");

        public WeatherClient() : base()
        {
            m_httpClient = new HttpClient();
        }
        ~WeatherClient()
        {
            m_httpClient.Dispose();
        }

        async public override Task<PointF> GetCityInfoAsync(string country)
        {
            if (country.Length == 0)
            {
                throw new ArgumentException("Wrong country name: ", country);
            }
            country = country.ToUpper();
            var jsonResponse = await m_httpClient.GetStringAsync(generatePositionRequestUrl(country));
            var jsonData = JObject.Parse(jsonResponse);

            if (!jsonData.HasValues)
            {
                throw new ArgumentException("Wrong country name: ", country);
            }

            float lap = 0, lon = 0;
            bool found = false;
            try
            {
                var citiesList = jsonData.SelectToken("data");
                foreach (var cityData in citiesList)
                {
                    if (!float.TryParse(cityData.SelectToken("latitude").ToString(), out lap) ||
                     !float.TryParse(cityData.SelectToken("longitude").ToString(), out lon))
                    {
                        throw new ArgumentException("Wrong country name: ", country);
                    }

                    if (country.Contains(cityData.SelectToken("name").ToString().ToUpper()))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    throw new ArgumentException("Country's lap and lon not found: ", country);
                }
            }
            catch (NullReferenceException)
            {
                throw new ArgumentException("Wrong country name: ", country);
            }

            return new PointF(lap, lon);
        }

        async public override Task<List<Result>> GetWeatherInfoAsync(string city, int dayCount)
        {
            if (dayCount < 1 || dayCount > 5)
            {
                throw new ArgumentException($"Wrong days count: {dayCount}");
            }

            var coords = await GetCityInfoAsync(city);

            var result = new List<Result>();
            int tempFixCount = 0;
            long time = getCurrentTime();
            for (int i = 1; i <= dayCount; i++)
            {
                try
                {
                    Thread.Sleep(800);
                    var jsonResponse = await m_httpClient.GetStringAsync(generateOpenWeatherRequestUrl(coords, time - m_dayUnixTime*i));
                    result.Add(ParseJson(jsonResponse));
                }
                catch (HttpRequestException ex)
                {
                    if (ex.Message.EndsWith("400 (Bad Request)."))
                    {
                        i--;
                        tempFixCount++;
                    }
                    if (tempFixCount > 10)
                    {
                        throw new ArgumentException("Error, While trying to response data from OpenWeatherApi", ex);
                    }
                }
            }
            return result;
        }
        public override Result ParseJson(string jsonResponse)
        {
            var result = new Result();
            int hoursRecorded = 0;
            result.TemperatureMin = 100;
            result.TemperatureMax = -100;

            var jsonData = JObject.Parse(jsonResponse);
            if (!jsonData.HasValues)
            {
                throw new ArgumentException("Empty Response data");
            }
            var jsonDataHourly = jsonData.SelectToken("hourly");
            if (!jsonDataHourly.HasValues)
            {
                throw new ArgumentException("Empty response for \"hourly\" token");
            }
            foreach (var jsonHourData in jsonDataHourly)
            {
                try
                {
                    const string cloudsString = "Clouds", rainString = "Rain";
                    hoursRecorded++;
                    if (result.Date != "")
                    {
                        long t = long.Parse(jsonHourData.SelectToken("dt").ToString());
                        result.Date = getDateTimeFromStamp(t).ToString("dd-MM-yyyy-HH-mm");
                    }
                    string weatherHorly = jsonHourData.SelectToken("weather").First.SelectToken("main").ToString();
                    if (weatherHorly == cloudsString)
                    {
                        result.Clouds += 1;
                    }
                    else if (weatherHorly == rainString)
                    {
                        result.Rain += 1;
                    }
                    
                    result.Pressure += int.Parse(jsonHourData.SelectToken("pressure").ToString());
                    var temperatureHourly = double.Parse(jsonHourData.SelectToken("temp").ToString());
                    result.Temperature += temperatureHourly;
                    result.TemperatureFeelsLike += double.Parse(jsonHourData.SelectToken("feels_like").ToString());
                    
                    if (temperatureHourly > result.TemperatureMax)
                    {
                        result.TemperatureMax = temperatureHourly;
                    }
                    if (temperatureHourly < result.TemperatureMin)
                    {
                        result.TemperatureMin = temperatureHourly;
                    }
                    result.WindSpeed += double.Parse(jsonHourData.SelectToken("wind_speed").ToString());
                }
                catch (NullReferenceException )
                {
                    throw new ArgumentException("Cannot parse received json data");
                }
            }
            
            result.Temperature = result.Temperature / hoursRecorded;
            result.WindSpeed = result.WindSpeed / hoursRecorded;
            result.Temperature = result.Temperature / hoursRecorded;
            result.TemperatureFeelsLike = result.TemperatureFeelsLike / hoursRecorded;
            result.Pressure = result.Pressure / hoursRecorded;

            return result;
        }
    }
}
