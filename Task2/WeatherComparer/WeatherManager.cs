﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace WeatherComparer
{
    class WeatherManager : IWeatherManager
    {
        private readonly WeatherClient m_weatherRequester;
        public WeatherManager() : base()
        {
            m_weatherRequester = new WeatherClient();
        }

        public override WeatherComparisonResults GetResults(string city1, string city2, int dayCount)
        {
            var infoCityTask1 = m_weatherRequester.GetWeatherInfoAsync(city1, dayCount);
            var infoCityTask2 = m_weatherRequester.GetWeatherInfoAsync(city2, dayCount);

            infoCityTask1.Wait();
            infoCityTask2.Wait();

            var infoCity1 = infoCityTask1.Result;
            var infoCity2 = infoCityTask2.Result;

            if (infoCity1.Count != infoCity2.Count)
            {
                throw new ArgumentException("Weather resaults count is not equal");
            }
            return new WeatherComparisonResults(infoCity1, infoCity2, city1, city2);
        }
    }
}
