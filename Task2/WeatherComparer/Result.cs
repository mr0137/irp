﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherComparer
{
    public class Result
    {
        public string Date { get; set; }
        public int Pressure { get; set; }
        public double Temperature { get; set; }
        public double TemperatureFeelsLike { get; set; }
        public double TemperatureMin { get; set; }
        public double TemperatureMax { get; set; }
        public double WindSpeed { get; set; }
        public int Clouds { get; set; }
        public int Rain { get; set ; }
    }
}
