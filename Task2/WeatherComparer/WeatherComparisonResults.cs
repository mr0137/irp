﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace WeatherComparer
{
    using Results = List<Result>;
    public class WeatherComparisonResults
    {
        private Results m_resultCity1;
        private Results m_resultCity2;
        private string m_cityName1;
        private string m_cityName2;

        public WeatherComparisonResults(Results cityResult1, Results cityResult2, string cityName1, string cityName2)
        {
            m_resultCity1 = cityResult1;
            m_resultCity2 = cityResult2;
            m_cityName1 = cityName1;
            m_cityName2 = cityName2;
        }

        public enum City
        {
            FIRST,
            SECOND
        }      

        public Results GetResult(City city)
        {
            if (city == City.FIRST)
            {
                return m_resultCity1;
            }
            else
            {
                return m_resultCity2;
            }
        }

        public string GetCityName(City city)
        {
            if (city == City.FIRST)
            {
                return m_cityName1;
            }
            else
            {
                return m_cityName2;
            }
        }
    }
}
