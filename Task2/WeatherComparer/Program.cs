﻿using System;
using System.Net;
using System.Net.Http;
using System.Xml.Linq;

namespace WeatherComparer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var manager = new WeatherManager();
                var results = manager.GetResults("London,United Kingdom", "Kiyv,Ukraine", 5);

                ResultsComparer.CompareRainerDays(results);

                ResultsComparer.CompareWarmerDays(results);

                ResultsComparer.CompareDetailed(results);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            
            Console.WriteLine("");
            Console.WriteLine("Press a key to exit...");
            Console.ReadLine();
        }
    }
}
