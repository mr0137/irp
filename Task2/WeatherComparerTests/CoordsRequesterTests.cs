﻿using System;
using System.Drawing;
using WeatherComparer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WeatherComparerTests
{
    [TestClass]
    public class CoordsRequesterTests
    {
        [TestMethod]
        public void GetCoordsTest1()
        {
            var requester = new WeatherClient();
            var task = requester.GetCityInfoAsync("Lviv,Ukraine");
            task.Wait();

            var coords = task.Result;
            var expectedCoords = new PointF((float)49.84156, (float)24.0313931);

            Assert.AreEqual(expectedCoords, coords, "Coords are not equal");
        }

        [TestMethod]
        public void GetCoordsTest2()
        {
            var requester = new WeatherClient();
            var task = requester.GetCityInfoAsync("Berlin");
            task.Wait();

            var coords = task.Result;
            var expectedCoords = new PointF((float)52.5249329, (float)13.407032);

            Assert.AreEqual(expectedCoords, coords, "Coords are not equal");
        }

        [TestMethod]
        [ExpectedException(typeof(AggregateException))]
        public void GetCoordsTest3()
        {
            var requester = new WeatherClient();
            var task = requester.GetCityInfoAsync("Berlin,Ukraine");
            task.Wait();
        }
    }
}
