﻿using System;
using System.Text;
using System.Drawing;
using WeatherComparer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WeatherComparerTests
{
    [TestClass]
    public class OpenWeatherRequesterTests
    {
        [TestMethod]
        [ExpectedException(typeof(AggregateException))]
        public void getWeatherInfoTest()
        {
            var requester = new WeatherClient();
            var task = requester.GetWeatherInfoAsync("Some city", 10);
            task.Wait();
        }

        [TestMethod]
        public void getDateTimeFromStampTest()
        {
            var dateTime = WeatherClient.getDateTimeFromStamp(1648456243);
            string dateTimeString = dateTime.ToString("dd.MM.yyyy");
            string expectedDateTime = "28.03.2022";

            Assert.AreEqual(expectedDateTime, dateTimeString, "DateTime doesn't equal expected");
        }

        [TestMethod]
        public void parseTest()
        {
            var requester = new WeatherClient();
            var builder = new StringBuilder();
            builder.Append("{\"lat\":49.8416,\"lon\":24.0314,\"timezone\":\"Europe/Kiev\",\"timezone_offset\":10800,");
            builder.Append("\"current\":{\"dt\":1648377480,\"sunrise\":1648354316,\"sunset\":1648399589,\"temp\":6.05,\"feels_like\":2.07,\"pressure\":1031,\"humidity\":31,\"dew_point\":-8.72,\"uvi\":3.32,\"clouds\":47,\"visibility\":10000,\"wind_speed\":6.43,\"wind_deg\":329,\"wind_gust\":8.47,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03d\"}]},");
            builder.Append("\"hourly\":[{\"dt\":1648339200,\"temp\":2.62,\"feels_like\":-2.55,\"pressure\":1026,\"humidity\":54,\"dew_point\":-5.1,\"uvi\":0,\"clouds\":0,\"visibility\":10000,\"wind_speed\":7.01,\"wind_deg\":326,\"wind_gust\":12.54,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clearsky\",\"icon\":\"01n\"}]},");
            builder.Append("{\"dt\":1648342800,\"temp\":1.61,\"feels_like\":-3.72,\"pressure\":1026,\"humidity\":56,\"dew_point\":-5.51,\"uvi\":0,\"clouds\":0,\"visibility\":10000,\"wind_speed\":6.7,\"wind_deg\":321,\"wind_gust\":12.52,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clearsky\",\"icon\":\"01n\"}]},");
            builder.Append("{\"dt\":1648346400,\"temp\":0.86,\"feels_like\":-4.56,\"pressure\":1027,\"humidity\":58,\"dew_point\":-5.73,\"uvi\":0,\"clouds\":3,\"visibility\":10000,\"wind_speed\":6.42,\"wind_deg\":319,\"wind_gust\":12.29,\"weather\":[{\"id\":800,\"main\":\"Rain\",\"description\":\"clearsky\",\"icon\":\"01n\"}]},");
            builder.Append("{\"dt\":1648350000,\"temp\":0.24,\"feels_like\":-5.25,\"pressure\":1028,\"humidity\":58,\"dew_point\":-6.25,\"uvi\":0,\"clouds\":3,\"visibility\":10000,\"wind_speed\":6.21,\"wind_deg\":322,\"wind_gust\":12.05,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clearsky\",\"icon\":\"01n\"}]},");
            builder.Append("{\"dt\":1648353600,\"temp\":-0.32,\"feels_like\":-5.72,\"pressure\":1029,\"humidity\":59,\"dew_point\":-6.56,\"uvi\":0,\"clouds\":3,\"visibility\":10000,\"wind_speed\":5.73,\"wind_deg\":317,\"wind_gust\":11.15,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clearsky\",\"icon\":\"01n\"}]},");
            builder.Append("{\"dt\":1648357200,\"temp\":-0.49,\"feels_like\":-5.79,\"pressure\":1030,\"humidity\":58,\"dew_point\":-6.92,\"uvi\":0.15,\"clouds\":5,\"visibility\":10000,\"wind_speed\":5.47,\"wind_deg\":318,\"wind_gust\":11.19,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clearsky\",\"icon\":\"01d\"}]},");
            builder.Append("{\"dt\":1648360800,\"temp\":0.54,\"feels_like\":-5.03,\"pressure\":1030,\"humidity\":51,\"dew_point\":-7.49,\"uvi\":0.55,\"clouds\":31,\"visibility\":10000,\"wind_speed\":6.56,\"wind_deg\":322,\"wind_gust\":10.81,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03d\"}]},");
            builder.Append("{\"dt\":1648364400,\"temp\":1.63,\"feels_like\":-3.66,\"pressure\":1031,\"humidity\":44,\"dew_point\":-8.28,\"uvi\":1.1,\"clouds\":39,\"visibility\":10000,\"wind_speed\":6.62,\"wind_deg\":328,\"wind_gust\":9.4,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03d\"}]},");
            builder.Append("{\"dt\":1648368000,\"temp\":2.66,\"feels_like\":-2.27,\"pressure\":1031,\"humidity\":40,\"dew_point\":-8.53,\"uvi\":1.9,\"clouds\":69,\"visibility\":10000,\"wind_speed\":6.46,\"wind_deg\":331,\"wind_gust\":8.83,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"brokenclouds\",\"icon\":\"04d\"}]},");
            builder.Append("{\"dt\":1648371600,\"temp\":3.85,\"feels_like\":-0.66,\"pressure\":1031,\"humidity\":36,\"dew_point\":-8.77,\"uvi\":2.63,\"clouds\":76,\"visibility\":10000,\"wind_speed\":6.23,\"wind_deg\":326,\"wind_gust\":8.3,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"brokenclouds\",\"icon\":\"04d\"}]},");
            builder.Append("{\"dt\":1648375200,\"temp\":5.14,\"feels_like\":0.91,\"pressure\":1031,\"humidity\":33,\"dew_point\":-8.73,\"uvi\":3.37,\"clouds\":57,\"visibility\":10000,\"wind_speed\":6.41,\"wind_deg\":329,\"wind_gust\":8.52,\"weather\":[{\"id\":803,\"main\":\"Rain\",\"description\":\"brokenclouds\",\"icon\":\"04d\"}]},");
            builder.Append("{\"dt\":1648378800,\"temp\":6.05,\"feels_like\":2.07,\"pressure\":1031,\"humidity\":31,\"dew_point\":-8.72,\"uvi\":3.32,\"clouds\":47,\"visibility\":10000,\"wind_speed\":6.43,\"wind_deg\":329,\"wind_gust\":8.47,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03d\"}]},");
            builder.Append("{\"dt\":1648382400,\"temp\":6.82,\"feels_like\":3.06,\"pressure\":1030,\"humidity\":30,\"dew_point\":-8.49,\"uvi\":2.77,\"clouds\":46,\"visibility\":10000,\"wind_speed\":6.39,\"wind_deg\":326,\"wind_gust\":8.37,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03d\"}]},");
            builder.Append("{\"dt\":1648386000,\"temp\":7.24,\"feels_like\":3.61,\"pressure\":1030,\"humidity\":29,\"dew_point\":-8.55,\"uvi\":1.97,\"clouds\":58,\"visibility\":10000,\"wind_speed\":6.37,\"wind_deg\":326,\"wind_gust\":8.35,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"brokenclouds\",\"icon\":\"04d\"}]},");
            builder.Append("{\"dt\":1648389600,\"temp\":7.22,\"feels_like\":3.58,\"pressure\":1029,\"humidity\":29,\"dew_point\":-8.56,\"uvi\":1.08,\"clouds\":45,\"visibility\":10000,\"wind_speed\":6.36,\"wind_deg\":331,\"wind_gust\":8.34,\"weather\":[{\"id\":802,\"main\":\"Rain\",\"description\":\"scatteredclouds\",\"icon\":\"03d\"}]},");
            builder.Append("{\"dt\":1648393200,\"temp\":6.89,\"feels_like\":3.35,\"pressure\":1029,\"humidity\":32,\"dew_point\":-7.7,\"uvi\":0.43,\"clouds\":61,\"visibility\":10000,\"wind_speed\":5.86,\"wind_deg\":333,\"wind_gust\":8.06,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"brokenclouds\",\"icon\":\"04d\"}]},");
            builder.Append("{\"dt\":1648396800,\"temp\":5.67,\"feels_like\":2.39,\"pressure\":1030,\"humidity\":37,\"dew_point\":-7,\"uvi\":0.1,\"clouds\":52,\"visibility\":10000,\"wind_speed\":4.56,\"wind_deg\":335,\"wind_gust\":7.87,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"brokenclouds\",\"icon\":\"04d\"}]},");
            builder.Append("{\"dt\":1648400400,\"temp\":3.52,\"feels_like\":0.81,\"pressure\":1030,\"humidity\":44,\"dew_point\":-6.73,\"uvi\":0,\"clouds\":47,\"visibility\":10000,\"wind_speed\":2.89,\"wind_deg\":331,\"wind_gust\":4.82,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03n\"}]},");
            builder.Append("{\"dt\":1648404000,\"temp\":2.91,\"feels_like\":0.4,\"pressure\":1030,\"humidity\":48,\"dew_point\":-6.23,\"uvi\":0,\"clouds\":15,\"visibility\":10000,\"wind_speed\":2.53,\"wind_deg\":319,\"wind_gust\":2.68,\"weather\":[{\"id\":801,\"main\":\"Rain\",\"description\":\"fewclouds\",\"icon\":\"02n\"}]},");
            builder.Append("{\"dt\":1648407600,\"temp\":2.48,\"feels_like\":0.3,\"pressure\":1030,\"humidity\":50,\"dew_point\":-6.11,\"uvi\":0,\"clouds\":33,\"visibility\":10000,\"wind_speed\":2.12,\"wind_deg\":306,\"wind_gust\":2.17,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03n\"}]},");
            builder.Append("{\"dt\":1648411200,\"temp\":2.24,\"feels_like\":0.52,\"pressure\":1031,\"humidity\":51,\"dew_point\":-6.08,\"uvi\":0,\"clouds\":37,\"visibility\":10000,\"wind_speed\":1.7,\"wind_deg\":292,\"wind_gust\":1.71,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03n\"}]},");
            builder.Append("{\"dt\":1648414800,\"temp\":2.01,\"feels_like\":0.7,\"pressure\":1030,\"humidity\":53,\"dew_point\":-5.82,\"uvi\":0,\"clouds\":46,\"visibility\":10000,\"wind_speed\":1.39,\"wind_deg\":264,\"wind_gust\":1.37,\"weather\":[{\"id\":802,\"main\":\"Rain\",\"description\":\"scatteredclouds\",\"icon\":\"03n\"}]},");
            builder.Append("{\"dt\":1648418400,\"temp\":1.75,\"feels_like\":0.32,\"pressure\":1030,\"humidity\":54,\"dew_point\":-5.82,\"uvi\":0,\"clouds\":41,\"visibility\":10000,\"wind_speed\":1.45,\"wind_deg\":238,\"wind_gust\":1.41,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03n\"}]},");
            builder.Append("{\"dt\":1648422000,\"temp\":1.33,\"feels_like\":-1.02,\"pressure\":1030,\"humidity\":55,\"dew_point\":-5.96,\"uvi\":0,\"clouds\":36,\"visibility\":10000,\"wind_speed\":2.1,\"wind_deg\":215,\"wind_gust\":2.03,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03n\"}]}]}");
            var result = requester.ParseJson(builder.ToString());

            Assert.AreEqual(5, result.Rain, "Parsed parameter dousn't equal to expexted");
        }

        [TestMethod]
        public void parseTest2()
        {
            var requester = new WeatherClient();
            var builder = new StringBuilder();
            builder.Append("{\"lat\":49.8416,\"lon\":24.0314,\"timezone\":\"Europe/Kiev\",\"timezone_offset\":10800,");
            builder.Append("\"current\":{\"dt\":1648377480,\"sunrise\":1648354316,\"sunset\":1648399589,\"temp\":6.05,\"feels_like\":2.07,\"pressure\":1031,\"humidity\":31,\"dew_point\":-8.72,\"uvi\":3.32,\"clouds\":47,\"visibility\":10000,\"wind_speed\":6.43,\"wind_deg\":329,\"wind_gust\":8.47,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03d\"}]},");
            builder.Append("\"hourly\":[{\"dt\":1648339200,\"temp\":2.62,\"feels_like\":-2.55,\"presure\":1026,\"humidity\":54,\"dew_point\":-5.1,\"uvi\":0,\"clouds\":0,\"visibility\":10000,\"wind_speed\":7.01,\"wind_deg\":326,\"wind_gust\":12.54,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clearsky\",\"icon\":\"01n\"}]},");
            builder.Append("{\"dt\":1648342800,\"temp\":1.61,\"feels_like\":-3.72,\"pressure\":1026,\"humidity\":56,\"dew_point\":-5.51,\"uvi\":0,\"clouds\":0,\"visibility\":10000,\"wind_speed\":6.7,\"wind_deg\":321,\"wind_gust\":12.52,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clearsky\",\"icon\":\"01n\"}]},");
            builder.Append("{\"dt\":1648418400,\"temp\":1.75,\"feels_like\":0.32,\"pressure\":1030,\"humidity\":54,\"dew_point\":-5.82,\"uvi\":0,\"clouds\":41,\"visibility\":10000,\"wind_speed\":1.45,\"wind_deg\":238,\"wind_gust\":1.41,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03n\"}]},");
            builder.Append("{\"dt\":1648422000,\"temp\":1.33,\"feels_like\":-1.02,\"pressure\":1030,\"humidity\":55,\"dew_point\":-5.96,\"uvi\":0,\"clouds\":36,\"visibility\":10000,\"wind_speed\":2.1,\"wind_deg\":215,\"wind_gust\":2.03,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scatteredclouds\",\"icon\":\"03n\"}]}]}");
            try
            {
                var result = requester.ParseJson(builder.ToString());
                Assert.Fail();
            }
            catch (ArgumentException)
            {

            }
        }
    }
}
